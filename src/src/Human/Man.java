package Human;

public class Man {
    static Man man;
    public static String name = "Rodion";
    public static int age = 24;
    public static String mr = "male";
    public static int weight = 81;


    public static Man getMan() {
        return man;
    }

    public static void setMan(Man man) {
        Man.man = man;
    }

    public String getName(String name) {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMr() {
        return mr;
    }

    public void setMr(String mr) {
        this.mr = mr;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    public static void main(String[] args) {


    }


}
