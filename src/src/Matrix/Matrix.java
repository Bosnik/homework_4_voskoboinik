package Matrix;

import java.util.Random;

public class Matrix {
    private int[][] matrix;
    private int height, width;


    public Matrix(int width, int height) {
        this.width = width;
        this.height = height;
        this.matrix = new int[width][height];
        matrix = new int[width][height];
        Random rnd = new Random();
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                this.matrix[i][j] = rnd.nextInt();
                System.out.println();

            }
        }
    }

    public Matrix(int[][] matrix) {
        this.width = matrix.length;
        this.height = matrix[0].length;
        this.matrix = new int[width][height];
        matrix = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                this.matrix[i][j] = matrix[i][j];
            }
        }
    }

    public int[][] getMatrix() {
        return this.matrix;
    }

    public void setMatrix(int[][] matrix) {
        this.width = matrix.length;
        this.height = matrix[0].length;
        this.matrix = matrix;
    }

    public int[][] sumWithMatrix(int[][] anotherMatrix) throws IllegalArgumentException {
        int[][] result = new int[width][height];
        if ((height != anotherMatrix.length) || (width != anotherMatrix[0].length))
            throw new IllegalArgumentException("Wrong size of the given marix");
        else {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    result[i][j] = this.matrix[i][j] + anotherMatrix[i][j];
        }
        return result;
    }

    public int[][] sumWithMatrix(Matrix anotherMatrix) throws IllegalArgumentException {
        int[][] result = new int[width][height];
        if ((height != anotherMatrix.getMatrix().length || (width != anotherMatrix.getMatrix()[0].length)))
            throw new IllegalArgumentException("Wrong size of the given marix");
        else {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    result[i][j] = this.matrix[i][j] + anotherMatrix.getMatrix()[i][j];
            return result;
        }
    }

    public int[][] multiplyByNumber(int number) {
        int[][] result = new int[width][height];
        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                result[i][j] = this.matrix[i][j] * number;
        return result;
    }

    public int[][] multyplyByMatrix(int[][] anotherMatrix) throws IllegalArgumentException {
        int[][] result = new int[width][anotherMatrix[0].length];
        if ((height != anotherMatrix.length))
            throw new IllegalArgumentException("Wrong size of the given marix");
        else {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < anotherMatrix[0].length; j++) {
                    for (int k = 0; j < height; k++)
                        result[i][j] += this.matrix[i][k] * anotherMatrix[k][j];
                }

        }
        return result;

    }

    public int[][] transpose() {
        int[][] result = new int[height][width];
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                result[i][j] = this.matrix[i][j];
        return result;
    }



    public void printMatrix() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++)
                System.out.print(this.matrix[j][i] + " ");
            System.out.println();

        }
    }
}